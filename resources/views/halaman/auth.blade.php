<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>

    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
    @csrf
   
    <label >First Name :</label><br>
    <input type="text" name="firstname"><br><br>
    <label >Last Name :</label><br>
    <input type="text" name="lastname"><br><br>
    <label >Gender :</label><br>
    <input type="radio" name="male">Man<br>
    <input type="radio" name="female">Woman<br>
    <input type="radio" name="other">Other<br><br>
    <label >Nationality :</label>
    <select name="nation">
        <option value="Indonesian">Indonesian</option>
        <option value="American">American</option>
        <option value="Korean">Korean</option>
        <option value="Japan">Japan</option>
    </select><br><br>
    <label >Language Spoken :</label><br>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Arabic<br>
    <input type="checkbox">Japanese <br><br>
    <label >Bio :</label><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="Sign Up">

    
    
    </form>
</body>
</html>