<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@auth');
Route::post('/welcome', 'AuthController@signup');



Route::get('/data-table',function(){
return view('table.data-table');
});

Route::get('/table',function(){
return view('table.table');
});

//crud cast
//untuk mengarah ke form tambah cast
Route::get('/cast/create','CastController@create');

//untuk insert data ke database
Route::post('/cast','CastController@store');

//mengambil semua data pada table cast
Route::get('/cast','CastController@index');

//menampilkan detail cast
Route::get('/cast/{cast_id}','CastController@show');

//mengarah ke form edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

//fungsi update berdasar id
Route::put('/cast/{cast_id}','CastController@update');

//fungsi delete berdasar id
Route::delete('/cast/{cast_id}','CastController@destroy');



