<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function auth()
    {
        return view("halaman.auth");
    }

    public function signup(Request $request)
    {
        // dd($request->all());
        $firstname = $request['firstname'];
        $lastname = $request ['lastname'];

        return view('halaman.welcome', compact('firstname', 'lastname'));
    }
}
